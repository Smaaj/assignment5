package com.example.assignment5;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class MainActivity extends AppCompatActivity {

    ProgressBar pBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pBar = (ProgressBar) findViewById(R.id.progressBar);
        pBar.setVisibility(View.VISIBLE);

        String uri = "https://api.opencagedata.com/geocode/v1/json?q=24.849535099999997," +
                "67.08595636543896&pretty=1&key=2272887cd1d547009306e11af65ece97";
        Ion.with(this)
                .load(uri)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject data) {
                        JsonArray jsonArray = data.getAsJsonArray("results");
                        JsonObject jsonObject = (JsonObject) jsonArray.get(0);
                        JsonObject annotations = jsonObject.getAsJsonObject("annotations");
                        JsonObject dms = annotations.getAsJsonObject("DMS");
                        String lat = dms.get("lat").toString();
                        String lng = dms.get("lng").toString();


                        if (e == null) {
                            pBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(MainActivity.this, lat + " " + lng, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        
                        }
                        
                        }

                });
                

    }
}